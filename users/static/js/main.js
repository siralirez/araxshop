var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        var dropdownContentChild = this.nextElementSibling.firstElementChild.offsetHeight;
        if (dropdownContent.style.height === dropdownContentChild + "px") {
            dropdownContent.style.height = "0px";
            this.firstElementChild.classList.add('fa-angle-left');
            this.firstElementChild.classList.remove('fa-angle-down');
        } else {
            dropdownContent.style.height = dropdownContentChild + "px";
            this.firstElementChild.classList.add('fa-angle-down');
            this.firstElementChild.classList.remove('fa-angle-left');
        }
    });
}

function openmenu(){
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function closemenu() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}
