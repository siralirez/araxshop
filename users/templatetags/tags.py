from django import template
from jalali_date import datetime2jalali, date2jalali
from datetime import datetime

register = template.Library()


@register.simple_tag()
def right_now():
    return datetime2jalali(datetime.now()).strftime('%H:%M:%S - %y/%m/%d')


@register.simple_tag()
def counter(lst):
    return len(lst)


@register.simple_tag()
def product_sum(factor):
    som = 0

    for i in factor:
        som += i.product.price * i.number
    return som

