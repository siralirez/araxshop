from django.contrib.auth import login as dj_login, logout as dj_logout
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from users.models import UploadToGalleryForm, UserGallery, SendTicket, Ticketing, UserInformation, AddProduct, Products, \
    AdminSendTicket, AddToFactor, Factor
from .forms import Login, Register, UserInfoForm


class login(View):
    def get(self, request):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return redirect('/admin-panel')
            return redirect('/user/main')
        next = request.GET.get('next', None)
        return render(request, 'login.html', {'next': next})

    def post(self, request):
        if request.user.is_authenticated:
            return redirect('/user/main')
        form = Login(request.POST)
        if form.is_valid():
            dj_login(request, form.user_cache)
            if request.GET.get('next', False):
                next = request.GET.get('next')
                return redirect(next)
            if request.user.is_superuser:
                return redirect('/admin-panel')
            return render(request, 'panel.html')
        return render(request, 'login.html', {'form': form})


class logout(View):
    def get(self, request):
        dj_logout(request)
        return redirect('/user/login')

    def post(self, request):
        dj_logout(request)
        return redirect('/user/login')


class register(View):
    def get(self, request):
        return render(request, 'register.html')

    def post(self, request):
        form = Register(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            User.objects.create_user(username, email=email,
                                     password=password)
            messages.add_message(request, messages.SUCCESS, 'با موفقیت ثبت نام شدید لطفا وارد شوید.')
            return redirect('/user/login')
        else:
            return render(request, 'register.html', {'form': form})


class forget_pass(View):
    def get(self, request):
        return render(request, 'forget-pass.html')

    def post(self, request):
        pass


class ticketing(LoginRequiredMixin, View):
    def get(self, request):
        tickets = Ticketing.objects.filter(user=request.user)
        return render(request, 'ticketing.html', {'tickets': tickets})

    def post(self, request):
        form_ticket = SendTicket(request.POST)
        if form_ticket.is_valid():
            ticket = form_ticket.save(commit=False)
            ticket.user = request.user
            ticket.answer = False
            ticket.save()
            messages.add_message(request, messages.SUCCESS, 'تیکت با موفقیت ثبت شد,از شکیبایی شمامتشکریم.')
            tickets = Ticketing.objects.filter(user=request.user)
            return render(request, 'ticketing.html', {'tickets': tickets})
        messages.add_message(request, messages.ERROR, 'ثبت تیکت با خطا مواجه شد لطفا بعدا تلاش نمایید.')
        tickets = Ticketing.objects.filter(user=request.user)
        return render(request, 'ticketing.html', {'tickets': tickets})


class gallery(LoginRequiredMixin, View):
    def get(self, request):
        gallery_items = UserGallery.objects.filter(user=request.user)
        return render(request, 'gallery.html', {'gallery': gallery_items})

    def post(self, request):
        form_img = UploadToGalleryForm(request.POST, request.FILES)
        if form_img.is_valid():
            image = form_img.save(commit=False)
            image.user = request.user
            image.save()
            messages.add_message(request, messages.SUCCESS, 'عکس با موفقیت آپلود شد.')
            gallery_items = UserGallery.objects.filter(user=request.user)
            return render(request, 'gallery.html', {'gallery': gallery_items})
        messages.add_message(request, messages.ERROR, 'مشکلی در آپلود عکس رخ داده است.')
        gallery_items = UserGallery.objects.filter(user=request.user)
        return render(request, 'gallery.html', {'gallery': gallery_items})


class edit_profile(LoginRequiredMixin, View):
    def get(self, request):
        info = UserInformation.objects.filter(user=request.user)
        return render(request, 'edit-profile.html', {'info': info})

    def post(self, request):
        form = UserInfoForm(request.POST)
        if form.is_valid():
            fname = form.cleaned_data['first_name']
            lname = form.cleaned_data['last_name']
            info = UserInformation.objects.filter(user=request.user)
            if not info:
                UserInformation.objects.create(user=request.user, first_name=fname, last_name=lname)
            else:
                UserInformation.objects.filter(user=request.user).update(first_name=fname, last_name=lname)
            messages.add_message(request, messages.SUCCESS, 'اطلاعات با موفقیت به روز رسانی شد.')
            return render(request, 'edit-profile.html')
        else:
            return render(request, 'edit-profile.html')


class buyed(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'buyed.html')

    def post(self, request):
        pass


class products(View):
    def get(self, request, group=False):
        print(group)
        if group:
            products = Products.objects.filter(group=group)
        else:
            products = Products.objects.all()
        return render(request, 'products.html', {'products': products})

    def post(self, request):
        pass


class product_show(View):
    def get(self, request, id):
        product = Products.objects.get(pk=id)
        return render(request, 'product-show.html', {'product': product})

    def post(self, request):
        pass


class factor(LoginRequiredMixin, View):
    def get(self, request):
        factor = Factor.objects.filter(user=request.user)
        return render(request, 'factor.html', {'factor': factor})

    def post(self, request):
        pass


class deletefromfactor(LoginRequiredMixin, View):
    def get(self, request, id):
        Factor.objects.get(pk=id).delete()
        messages.add_message(request, messages.SUCCESS, 'کالا از سبد حذف شد.')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def post(self, request):
        pass


class addtofactor(LoginRequiredMixin, View):
    def get(self, request):
        pass

    def post(self, request):
        form = AddToFactor(request.POST)
        if form.is_valid():
            addfactor = form.save(commit=False)
            addfactor.user = request.user
            addfactor.save()
            messages.add_message(request, messages.SUCCESS, 'کالا به سبد خرید اضافه شد.')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        messages.add_message(request, messages.ERROR, 'اضافه کردن کالا به سبد خرید با خطا مواجه شد.')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class user_panel(LoginRequiredMixin, View):
    # don't delete this
    def get(self, request):
        return render(request, 'panel.html')

    def post(self, request):
        pass


class products_list(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        products = Products.objects.all()
        return render(request, 'products-list.html', {'products': products})

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class addProduct(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        return render(request, 'addProduct.html')

    def post(self, request):
        form = AddProduct(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'محصول با موفقیت ثبت شد.')
        return render(request, 'addProduct.html', {'form': form})

    def test_func(self):
        return self.request.user.is_staff


class editProduct(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        return render(request, 'editProduct.html')

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class purchase_list(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        return render(request, 'purchase-list.html')

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class users_list(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        users = User.objects.filter(is_staff=False)
        return render(request, 'users-list.html', {'users': users})

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class admin_ticketing(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request, user_id):
        tickets = Ticketing.objects.filter(user=user_id)
        return render(request, 'admin-ticketing.html', {'tickets': tickets, 'user_id': user_id})

    def post(self, request):
        form_ticket = AdminSendTicket(request.POST)
        if form_ticket.is_valid():
            answer = form_ticket.save(commit=False)
            answer.answer = 1
            answer.save()
            user = form_ticket.cleaned_data.get('user')
            messages.add_message(request, messages.SUCCESS, 'پاسخ با موفقیت ثبت شد.')
            return redirect('/ticketing/{}'.format(user.id))
        messages.add_message(request, messages.ERROR, 'ثبت پاسخ با خطا مواجه شد.')
        return redirect('/tickets_list')

    def test_func(self):
        return self.request.user.is_staff


class admin_ticket_list(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        tickets = Ticketing.objects.all()
        counter = 0
        return render(request, 'admin-ticket_list.html', {'tickets': tickets, 'counter': counter})

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class admin_panel(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        return render(request, 'admin-panel.html')

    def post(self, request):
        pass

    def test_func(self):
        return self.request.user.is_staff


class index(View):
    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        pass


class aboutUs(View):
    def get(self, request):
        return render(request, 'aboutUs.html')

    def post(self, request):
        pass


class whyUs(View):
    def get(self, request):
        return render(request, 'whyUs.html')

    def post(self, request):
        pass
