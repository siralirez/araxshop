from django.contrib import admin
from users.models import *

admin.site.register(UserInformation)
admin.site.register(UserGallery)
admin.site.register(Ticketing)
admin.site.register(Products)
admin.site.register(Factor)
admin.site.register(Payed)

