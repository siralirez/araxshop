from django.urls import path
from users import views

urlpatterns = [
    path('login', views.login.as_view()),
    path('logout', views.logout.as_view()),
    path('forget-password', views.forget_pass.as_view()),
    path('register', views.register.as_view()),
    path('ticketing', views.ticketing.as_view()),
    path('gallery', views.gallery.as_view()),
    path('buyed', views.buyed.as_view()),
    path('factor', views.factor.as_view()),
    path('edit-profile', views.edit_profile.as_view()),
    path('main', views.user_panel.as_view()),
    path('addtofactor', views.addtofactor.as_view()),
    path('deletefromfactor/<int:id>', views.deletefromfactor.as_view()),

]
