"""AraksShop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/u/rls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from users import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # pub urls
    path('user/', include('users.urls')),
    path('', views.index.as_view()),
    path('aboutUs', views.aboutUs.as_view()),
    path('whyUs', views.whyUs.as_view()),
    path('products', views.products.as_view()),
    path('products/<str:group>', views.products.as_view()),
    path('product-show/<int:id>', views.product_show.as_view()),
    path('logout', views.logout.as_view()),
    path('login', views.login.as_view()),

    # admin urls
    path('admin-panel', views.admin_panel.as_view()),
    path('users-list', views.users_list.as_view()),
    path('products_list', views.products_list.as_view()),
    path('purchase_list', views.purchase_list.as_view()),
    path('addProduct', views.addProduct.as_view()),
    path('editProduct', views.editProduct.as_view()),
    path('ticketing', views.admin_ticketing.as_view()),
    path('ticketing/<int:user_id>', views.admin_ticketing.as_view()),
    path('tickets_list', views.admin_ticket_list.as_view()),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
